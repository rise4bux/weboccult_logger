import 'dart:developer';
import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:weboccult_logger/weboccult_logger.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  late Logger logger;
  @override
  void initState() {
    initLogger();
    super.initState();
  }

  initLogger() async {
    await Firebase.initializeApp();
    Directory dir = await getApplicationDocumentsDirectory();
    logger = Logger(File(dir.path + '/log2.txt'), fireBase: true);
  }

  void _incrementCounter() {
    // File file = File(
    //     '/data/user/0/com.example.weboccult_logger/app_flutter/log.txt');
    // log(file.readAsStringSync());
    setState(() {
      _counter++;
    });
    logger.info(
      _counter.toString(),
      tag: "debugging",
      uploadToServer: false,
    );
    logger.verbose(
      _counter.toString(),
      tag: "debugging",
      uploadToServer: false,
    );
    logger.debug(
      _counter.toString(),
      tag: "debugging",
      uploadToServer: false,
    );
    logger.warning(
      _counter.toString(),
      tag: "debugging",
      uploadToServer: false,
    );
    logger.error(
      _counter.toString(),
      tag: "debugging",
      uploadToServer: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

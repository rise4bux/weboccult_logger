import 'dart:io';

import 'package:intl/intl.dart';
import 'package:weboccult_logger/src/emitter/file_emitter.dart';
import 'package:weboccult_logger/src/formatter/formatter.dart';
import 'package:weboccult_logger/src/handler.dart';
import 'package:weboccult_logger/src/level.dart';
import 'package:weboccult_logger/src/record.dart';

/// Dart log.
///
/// [message] The message to output.
/// [tag] Optional. Log tag.
/// [title] Optional. Line shows above [message].
/// [stackTrace] Optional. StackTrace shows below [message].
class Logger {
  /// Specify [level] to [Level.off] to disable all output.
  Level level = Level.all;

  final Set<Handler> _handlers = {};

  Logger(File? file, {bool? fireBase}) {
    _handlers.add(Handler(
        formatter: SimpleFormatter(),
        emitter: FileEmitter(file: file!, append: true, firebase: fireBase!)));
  }

  void verbose(dynamic message,
      {String? tag,
      String? title,
      StackTrace? stackTrace,
      bool? uploadToServer}) {
    _log(Level.verbose, message,
        tag: tag, title: title, stackTrace: stackTrace, upload: uploadToServer);
  }

  void debug(dynamic message,
      {String? tag,
      String? title,
      StackTrace? stackTrace,
      bool? uploadToServer}) {
    _log(Level.debug, message,
        tag: tag, title: title, stackTrace: stackTrace, upload: uploadToServer);
  }

  void info(dynamic message,
      {String? tag,
      String? title,
      StackTrace? stackTrace,
      bool? uploadToServer}) {
    _log(Level.info, message,
        tag: tag, title: title, stackTrace: stackTrace, upload: uploadToServer);
  }

  void warning(dynamic message,
      {String? tag,
      String? title,
      StackTrace? stackTrace,
      bool? uploadToServer}) {
    _log(Level.warning, message,
        tag: tag, title: title, stackTrace: stackTrace, upload: uploadToServer);
  }

  void error(dynamic message,
      {String? tag,
      String? title,
      StackTrace? stackTrace,
      bool? uploadToServer}) {
    _log(Level.error, message,
        tag: tag, title: title, stackTrace: stackTrace, upload: uploadToServer);
  }

  void _log(Level level, dynamic message,
      {String? tag,
      String? title,
      StackTrace? stackTrace,
      bool? upload = true}) {
    if (level < this.level) {
      return;
    }
    Record record =
        Record(level, message, DateTime.now(), tag, title, stackTrace);
    for (Handler handler in _handlers) {
      handler.handle(record, upload!);
    }
  }
}

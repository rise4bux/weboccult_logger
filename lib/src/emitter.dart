import 'package:weboccult_logger/src/record.dart';

/// Emit log.
abstract class Emitter {
  void emit(Record record, List<String> lines, bool? uploadToServer);

  void destroy() {}
}

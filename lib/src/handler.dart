import 'package:weboccult_logger/src/emitter.dart';
import 'package:weboccult_logger/src/formatter.dart';
import 'package:weboccult_logger/src/record.dart';

/// Log handler.
class Handler {
  final Formatter formatter;
  final Emitter emitter;

  Handler({
    required this.formatter,
    required this.emitter,
  });

  void handle(Record record, bool upload) {
    List<String> lines = formatter.format(record);
    emitter.emit(record, lines, upload);
  }

  void destroy() {
    emitter.destroy();
  }
}

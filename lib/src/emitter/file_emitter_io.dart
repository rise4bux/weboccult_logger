import 'dart:developer';
import 'dart:io';

import 'package:ansicolor/ansicolor.dart';
import 'package:path/path.dart';
import 'package:weboccult_logger/src/emitter.dart';
import 'package:weboccult_logger/src/level.dart';
import 'package:weboccult_logger/src/record.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

/// Write log to file.
class FileEmitter extends Emitter {
  /// The file to store log.
  final File file;
  final bool firebase;

  /// [FileMode.writeOnlyAppend] or [FileMode.writeOnly].
  final bool append;

  IOSink? _ioSink;
  final Map levelColors = {
    Level.verbose: 008, // gray
    Level.debug: 006, // cyan
    Level.info: 007, // white
    Level.warning: 003, // yellow
    Level.error: 001, // red
  };

  final AnsiPen pen = AnsiPen();
  FileEmitter({
    required this.file,
    this.append = true,
    this.firebase = false,
  }) {
    _ioSink = file.openWrite(
        mode: append ? FileMode.writeOnlyAppend : FileMode.writeOnly);
  }

  @override
  void emit(Record record, List<String> lines, bool? uploadToServer) {
    ansiColorDisabled = false;
    pen.reset();
    if (levelColors[record.level] != null) {
      pen.xterm(levelColors[record.level]);
    }
    for (String line in lines) {
      print(pen(line));
      _ioSink?.writeln(line);
    }
    if (uploadToServer! && firebase) {
      uploadString();
    }
  }

  /// A new string is uploaded to storage.
  firebase_storage.UploadTask uploadString() {
    String fileName = basename(file.path);

    // Create a Reference to the file
    firebase_storage.Reference ref = firebase_storage.FirebaseStorage.instance
        .ref()
        .child('logs')
        .child('$fileName');

    // Start upload of putString
    return ref.putFile(file);
  }

  @override
  void destroy() async {
    await _ioSink?.flush();
    await _ioSink?.close();
  }
}

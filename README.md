# weboccult_logger

[![Pub Version](https://img.shields.io/pub/v/weboccult_logger)](https://pub.dev/packages/weboccult_logger)

Simple and pretty log package for Dart, includes Flutter and web.
You can also add log to your server.

## Getting Started

### Install

```yaml
dependencies:
  weboccult_logger: any # replace 'any' with version number.
```

```dart
import 'package:weboccult_logger/weboccult_logger.dart';
```

### Usage

```dart
// Init logger
late Logger logger;
// init your firebase app.
await Firebase.initializeApp();
Directory dir = await getApplicationDocumentsDirectory();
// firebase == true : it will upload logs to your firebase storage.
logger = Logger(File(dir.path + '/log.txt'), fireBase: true);


// simple log
// uploadToServer = true : it will upload all your updated logs to the storage

logger.info(
_counter.toString(),
tag: "debugging",
uploadToServer: false,
);
logger.verbose(
_counter.toString(),
tag: "debugging",
uploadToServer: false,
);
logger.debug(
_counter.toString(),
tag: "debugging",
uploadToServer: false,
);
logger.warning(
_counter.toString(),
tag: "debugging",
uploadToServer: false,
);
logger.error(
_counter.toString(),
tag: "debugging",
uploadToServer: true,
);
```
![Alt-Text](https://firebasestorage.googleapis.com/v0/b/logger-5ab41.appspot.com/o/1.png?alt=media&token=73336ed2-46f6-4e3a-b145-881c10d32de5)


```dart
// Exception/StackTrace
try {
  throw Exception('This is an exception.');
} catch (e, st) {
  logger.error(_counter.toString(),
      tag: "debugging", uploadToServer: true, stackTrace: st);
}
```
![Alt-Text](https://firebasestorage.googleapis.com/v0/b/logger-5ab41.appspot.com/o/2.png?alt=media&token=4e204c0f-2df7-4df7-89dd-a091ca60ecf3)
## On server uploaded log file
![Alt-Text](https://firebasestorage.googleapis.com/v0/b/logger-5ab41.appspot.com/o/3.png?alt=media&token=813be2af-3298-4a8b-9495-9ac56fa64cab)

## License
Weboccult